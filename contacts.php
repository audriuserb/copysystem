<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<title>CopyX</title>
</head>
<body>
	<header>
		<div class="container_1">
			<div class="box_1">
				<img src="img/logo.jpg" name="logo" id="logo">
			</div>
			<div class="box_2">
				<input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
			</div>
			<div class="box_3">
				<button type="button" id="search_btn">Ieškoti</button>
			</div>
		</div>
		<center>
			<nav id="navigacija">
				<a href="index.php">Formos ir ataskaitos</a> |
				<a href="information.php">Informacija</a> |
				<a href="contacts.php">Kontaktai</a>
			</nav>
		</center>
	</header>

		<main>
			<h1>Kontaktai</h1>
				<p> Technikos prižiūrėtojas:<br>
					+37054875321<br>
					priziuretojas@gmail.com
				</p>
				<p> Internetinės sistemos administratorius:<br>
					+370341564651<br>
					administratorius@gmail.com
				</p>
						
		</main>
	<footer>
		<h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
	</footer>
</body>
</html>