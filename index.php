<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<title>CopyX</title>
</head>
<body>
	<header>
		<div class="container_1">
			<div class="box_1">
				<img src="img/logo.jpg" name="logo" id="logo">
			</div>
			<div class="box_2">
				<input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
			</div>
			<div class="box_3">
				<button type="button" id="search_btn">Ieškoti</button>
			</div>
		</div>
		<center>
			<nav id="navigacija">
				<a href="index.php">Formos ir ataskaitos</a> |
				<a href="information.php">Informacija</a> |
				<a href="contacts.php">Kontaktai</a>
			</nav>
		</center>
	</header>

		<main>
			<h1>Formos ir ataskaitos</h1>
			<div class="container_2">
				<div class="element_1" style="background-color: silver;">
					<a href="form_1.php">Dienos apskaita</a>
				</div>
				<div class="element_2">
					<a href="form_2.php">Grynų pinigų įnešimas pagal darbuotoją</a>
				</div>
				<div class="element_3">
					<a href="form_3.php">Firmos sąskaitos už mėnesį</a>
				</div >
				<div class="element_4">
					<a href="form_4.php">Firmos aptarnavimas pagal dieną</a>
				</div>
				<div class="element_5">
					<a href="form_5.php">Menesio darbo suvestine</a>
				</div>
			</div>	
		</main>
	<footer>
		<h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
	</footer>
</body>
</html>