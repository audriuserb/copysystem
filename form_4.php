<?php
$user = 'root';
$pass = '';

try
{
	$dbh = new PDO('mysql:host=localhost;dbname=copysystem', $user, $pass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	echo "<script>alert('Connected succesfully!');</script>";
}
catch(PDOException $e)
{
     echo "<script>alert('Connection failed!');</script>";
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/normalize.css"> 
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<title>CopyX</title>
</head>
<body>

    <?php
    $select_sql = $dbh->query("SELECT * FROM employees ORDER BY id");
    $select_employee= $select_sql->FetchAll();

    $select_sql = $dbh->query("SELECT * FROM companies ORDER BY id");
    $select_company= $select_sql->FetchAll();

    $select_sql = $dbh->query("SELECT orders.id, orders.date, orders.pages_printed, orders.total,orders.company_total, CONCAT(employees.name,' ',employees.surname), companies.title, order_statuses.title, orders.created_at, orders.updated_at , employees.id as 'employee_id', companies.id as 'company_id', order_statuses.id as 'order_status_id' FROM orders INNER JOIN employees on employees.id = orders.employee_id inner join companies on companies.id = orders.company_id inner join order_statuses on order_statuses.id = orders.order_status GROUP BY orders.id ORDER BY id");
    $table= $select_sql->FetchAll();

    $select_sql = $dbh->query("SELECT * FROM order_statuses ORDER BY id");
    $select_status = $select_sql->FetchAll();

    $select_sql = $dbh->query("SELECT id FROM orders ORDER BY id");
    $select_order = $select_sql->FetchAll();


    $save = isset($_POST['save']) ? $_POST['save'] : '';
    $method = isset($_REQUEST['method']) ? $_REQUEST['method'] : '';
    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $employee_name=isset($_REQUEST['employee_name']) ? $_REQUEST['employee_name'] : '';
    $status_name=isset($_REQUEST['status_name']) ? $_REQUEST['status_name'] : '';
    $order_date=isset($_REQUEST['order_date']) ? $_REQUEST['order_date'] : '';
    $pages=isset($_REQUEST['pages']) ? $_REQUEST['pages'] : '';
    $money=isset($_REQUEST['money']) ? $_REQUEST['money'] : '';
    $company_name=isset($_REQUEST['company_name']) ? $_REQUEST['company_name'] : '';
    $company_total=isset($_REQUEST['company_total']) ? $_REQUEST['company_total'] : '';
    $company_id=isset($_REQUEST['company_id']) ? $_REQUEST['company_id'] : '';
    $employee_id=isset($_REQUEST['employee_id']) ? $_REQUEST['employee_id'] : '';
    $order_status_id=isset($_REQUEST['order_status_id']) ? $_REQUEST['order_status_id'] : '';
    if ($method =='' and $save) $method='save';

    //lenteles orders pildymas

    if($method == 'save')
    {
          $created_at = date('y-m-d h-i-se');
          $sql_1 = $dbh->prepare("INSERT INTO orders (date, pages_printed, total, company_total, employee_id, company_id, order_status, created_at) VALUES ('$_POST[order_date]', '$_POST[pages]', '$_POST[money]',  '$_POST[company_total]', '$_POST[employee_name]', '$_POST[company_name]', '$_POST[status_name]', '$created_at')");
          if($sql_1->execute())
          {
             echo "<script>alert('Duomenys išsaugoti');</script>";
             header("Refresh:0; url='form_4.php'");
          }
          else
          {
            echo "<script>alert('Duomenų išsaugoti nepavyko');</script>";
          }
    }
    //lentelės orders įrašų redagavimas
    elseif($method == 'PUT')
        {
        $updated_at = date('y-m-d h-i-se');
        $sql_2 = $dbh->query("UPDATE orders SET date = '$order_date', pages_printed = '$pages', total = '$money', employee_id = '$employee_name', company_id='$company_name', order_status = '$status_name', updated_at = '$updated_at', company_total = '$company_total' WHERE id = '$id'");
        if($sql_2->execute())
            {
            echo "<script>alert('Užsakymo duomenys pakeisti');</script>";
            header("Refresh:0; url='form_4.php'");
            }
        else 
            {
            echo "<script>alert('Užsakymo duomenų pakeisti nepavyko');</script>";
            }
        }

    //lentelės orders įrašų šalinimas
    else if ($method == 'delete')
    {
        $sql_3=$dbh->prepare("DELETE FROM orders where id = '$id'");
        if($sql_3->execute())
        {
            echo "<script>alert('Užsakymo duomenys ištrinti');</script>"; 
            header("Refresh:0; url='form_4.php'");
        }
        else
        {
            echo "<script>alert('Užsakymo duomenų ištrinti nepavyko');</script>";
        }
    }
    $dbh = null;
    ?>
	<header>
		<div class="container_1">
			<div class="box_1">
				<img src="img/logo.jpg" name="logo" id="logo">
			</div>
			<div class="box_2">
				<input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
			</div>
			<div class="box_3">
				<button type="button" id="search_btn">Ieškoti</button>
			</div>
		</div>
		<center>
			<nav id="navigacija">
				<a href="index.php">Formos ir ataskaitos</a> |
				<a href="information.php">Informacija</a> |
				<a href="contacts.php">Kontaktai</a>
			</nav>
		</center>
	</header>

		<main>
			<h1>Formos ir ataskaitos</h1>
			<div class="container_2">
				<div class="element_1">
					<a href="form_1.php">Įrašyti naują darbuotoją</a>
				</div>
				<div class="element_2">
					<a href="form_2.php">Įrašyti naują įmonę</a>
				</div>
				<div class="element_3">
					<a href="form_3.php">Įrašyti naują užsakymo būklę</a>
				</div>
				<div class="element_4" style="background-color: silver;">
					<a href="form_4.php">Dienos apskaita</a>
				</div>
				<div class="element_5">
					<a href="form_5.php">Grynų pinigų įnešimas pagal darbuotoją</a>
				</div>
				<div class="element_6">
					<a href="form_6.php">Firmos sąskaitos už mėnesį</a>
				</div >
				<div class="element_7">
					<a href="form_7.php">Firmos aptarnavimas pagal dieną</a>
				</div>
				<div class="element_8">
					<a href="form_8.php">Menesio darbo suvestine</a>
				</div>
			</div>
		
			<div class="container_3">
				<div><h3>Įvesti</h3></div>
				<form action="form_4.php<?php if($id) : echo '?id='.$id; endif;?>" method="<?php if($method=='edit') : echo 'get'; elseif($method=='PUT' || $method=='delete'): echo 'post'; else : echo 'post';endif;?>">
				<div>
                <?php 
                if($method == 'edit'):
                    echo '<input type="hidden" name="id" value="'.$id.'">';
                    echo '<input type="hidden" name="method" value="PUT" >';
                endif;
                ?>
				<select  name="employee_name" placeholder="Darbuotojo vardas">
						<?php foreach($select_employee as $v) : 
                        if($v[0] == $employee_id && $method == 'edit'):
    					echo '<option selected value="'.$id.'">'.$employee_name.'</option>';
                        else:
                        echo '<option value="'.$v[0].'">'.$v[1].' '.$v[2].'</option>';
                        endif;
    					endforeach; ?>
				</select>
    			</div>
    			<div>
    				<input type="date" name="order_date" value="<?php if($method == 'edit') echo $order_date; ?>">
    			</div>
    			<div>
    				<select  name="company_name" placeholder="Įmonės pavadinimas">
					<?php foreach($select_company as $v): 
                    if($v[0] == $company_id && $method == 'edit'):
                    echo '<option selected value="'.$id.'">'.$company_name.'</option>';
                    else:
					echo '<option value="'.$v[0].'">'.$v[1].'</option>';
                    endif;
    				endforeach; ?>
					</select>
    			</div>
    			<div>
    				<select  name="status_name" placeholder="Užsakymo būklės pavadinimas">
					<?php foreach($select_status as $v):
                    if($v[0] == $order_status_id && $method == 'edit'):
                    echo '<option selected value="'.$id.'">'.$status_name.'</option>';
                    else:
					echo '<option value="'.$v[0].'">'.$v[1].'</option>';
                    endif;
    				endforeach; ?>
					</select>
    			</div>
    			<div>
    				<input type="number" name="pages" placeholder="Lapų skaičius" value="<?php if($method == 'edit') echo $pages; ?>">
    				<input type="number" name="money" placeholder="Gauta pinigų suma" value="<?php if($method == 'edit') echo $money; ?>">
                    <input type="number" name="company_total" placeholder="Kompanijos užsakymo kaina" value="<?php if($method == 'edit') echo $company_total; ?>">
    			</div>
                <div>
                    <button type="submit" name="save" value="ok">Išsaugoti</button>
                </div>
    		</form>
			</div>		

			<div class="container_3">
    		<div><h3>Rezultatai</h3></div>
    		<div>
    		<table id='table_update'>
    			<tr><th>id</th><th>data</th><th>lapų skaičius</th><th>suma (už lapus)</th><th>suma (įmonės)</th><th>Darbuotojas</th><th>Įmonė</th><th>Užsakymo būklė</th><th>Sukurta</th><th>Atnaujinta</th></tr>

    		<?php
    		//Kodas skirtas spausdinti employees lentele
				foreach($table as $v)
				{
    				echo "<tr>";
    				echo "<td>{$v[0]}</td>";
   					echo "<td>{$v[1]}</td>";
   					echo "<td>{$v[2]}</td>";
   					echo "<td>{$v[3]}</td>";
   					echo "<td>{$v[4]}</td>";
                    echo "<td>{$v[5]}</td>";
   					echo "<td>{$v[6]}</td>";
   					echo "<td>{$v[7]}</td>";
   					echo "<td>{$v[8]}</td>";
   					echo "<td>{$v[9]}</td>";
                    echo '<td><a href="form_4.php?method=edit&id='.$v[0].'&order_date='.$v[1].'&pages='.$v[2].'&money='.$v[3].'&company_total='.$v[4].'&employee_name='.$v[5].'&company_name='.$v[6].'&status_name='.$v[7].'&employee_id='.$v[10].'&company_id='.$v[11].'&order_status_id='.$v[12].'">Redaguoti</a></td>';
                    echo '<td><a href="form_4.php?method=delete&id='.$v[0].'">Ištrinti</a></td>';
    				echo "</tr>";
				}
			?>

			</table>
			</div>
			</div>
		</main>
	<footer>
		<h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
	</footer>
</body>
</html>