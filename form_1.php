<?php
    $user = 'root';
    $pass = '';

    try
    {
        $dbh = new PDO('mysql:host=localhost;dbname=copysystem', $user, $pass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "<script>alert('Connected succesfully!');</script>";
    }
    catch(PDOException $e)
    {
         //echo "<script>alert('Connection failed!');</script>";
    }

    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <title>CopyX</title>
    </head>
    <body>
        <?php 

            $table_sql = $dbh->query("SELECT * FROM employees ORDER BY id");
            $table=$table_sql->FetchAll();
            
                $save = isset($_POST['save']) ? $_POST['save'] : '';
                $method = isset($_REQUEST['method']) ? $_REQUEST['method'] : '';
                $id = isset($_REQUEST['employee_id']) ? $_REQUEST['employee_id'] : '';
                $employee_name=isset($_REQUEST['employee_name']) ? $_REQUEST['employee_name'] : '';
                $employee_surname=isset($_REQUEST['employee_surname']) ? $_REQUEST['employee_surname'] : '';
                if ($method =='' and $save) $method='save';
                $s = $_SERVER['REQUEST_METHOD'];
                echo "<script>alert('$s');</script>";
            
                    //lenteles employees pildymas
                    if($method == 'save')
                    {
                    if($employee_name=='' || $employee_surname=='')
                    {
                    if($employee_name=='')
                    {
                    echo "<script>alert('Įveskite darbuotojo vardą.');</script>";
                    }
                    if($employee_surname=='')
                    {
                    echo "<script>alert('Įveskite darbuotojo pavardę.');</script>";    
                    }
                    }
                    else
                    {
                    $duplicate = $dbh->query("SELECT id FROM employees WHERE NOT EXISTS (SELECT id FROM employees WHERE name = '$employee_name' and surname = '$employee_surname' LIMIT 1)");
                    $result = $duplicate->fetch();
                    if($result[0] != 0)
                        {
                        $sql_1 = $dbh->prepare("INSERT INTO employees (name, surname)  Values ('$employee_name', '$employee_surname' )");
                        $sql_1->execute();
                        echo "<script>alert('Darbuotojo duomenys issaugoti');</script>";
                        header("Refresh:0; url='form_1.php'");
                        }
                    else
                        {
                        echo "<script>alert('Toks darbuotojas jau egzistuoja');</script>";
                        }
                    }
                    }
                    
            
            
            
                    //lentelės employees įrašų redagavimas
                    else if($method == 'update')
                    {
                    if($employee_name=='' || $employee_surname=='')
                    {
                    if($employee_name=='')
                    {
                    echo "<script>alert('Įveskite darbuotojo vardą.');</script>";
                    }
                    if($employee_surname=='')
                    {
                    echo "<script>alert('Įveskite darbuotojo pavardę.');</script>";    
                    }
                    }
                    else
                    {
                    $sql_2 = $dbh->prepare("UPDATE employees SET name = '$employee_name', surname = '$employee_surname' WHERE id = '$_GET[employee_id]' ");
                    if($sql_2->execute())
                        {
                        echo "<script>alert('Darbuotojo duomenys pakeisti');</script>";
                        header("Refresh:0; url='form_1.php'");
                        }
                    else 
                        {
                        echo "<script>alert('Darbuotojo duomenų pakeisti nepavyko');</script>";
                        }
                    }
                    }
                
                
                    //lentelės employees įrašų šalinimas
                    else if($method=='delete')
                    {
                        $sql_3=$dbh->prepare("DELETE FROM employees WHERE employees.id='$id'");
                        if($sql_3->execute())
                        {
                        echo "<script>alert('Darbuotojo duomenys ištrinti');</script>";
                        }
                        else
                        {
                        echo "<script>alert('Darbuotojo duomenų ištrinti nepavyko.');</script>";
                        }
                        header("Refresh:0; url='form_1.php'");
                    }
                    
                    $dbh=null;
                    ?>
        <header>
            <div class="container_1">
                <div class="box_1">
                    <img src="img/logo.jpg" name="logo" id="logo">
                </div>
                <div class="box_2">
                    <input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
                </div>
                <div class="box_3">
                    <button type="button" id="search_btn">Ieškoti</button>
                </div>
            </div>
            <center>
                <nav id="navigacija">
                    <a href="index.php">Formos ir ataskaitos</a> |
                    <a href="information.php">Informacija</a> |
                    <a href="contacts.php">Kontaktai</a>
                </nav>
            </center>
        </header>
        <main>
            <h1>Formos ir ataskaitos</h1>
            <div class="container_2">
                <div class="element_1" style="background-color: silver;">
                    <a href="form_1.php">Įrašyti naują darbuotoją</a>
                </div>
                <div class="element_2">
                    <a href="form_2.php">Įrašyti naują įmonę</a>
                </div>
                <div class="element_2">
                    <a href="form_3.php">Įrašyti naują užsakymo būklę</a>
                </div>
                <div class="element_3">
                    <a href="form_4.php">Dienos apskaita</a>
                </div>
                <div class="element_4">
                    <a href="form_5.php">Grynų pinigų įnešimas pagal darbuotoją</a>
                </div>
                <div class="element_5">
                    <a href="form_6.php">Firmos sąskaitos už mėnesį</a>
                </div >
                <div class="element_6">
                    <a href="form_7.php">Firmos aptarnavimas pagal dieną</a>
                </div>
                <div class="element_7">
                    <a href="form_8.php">Menesio darbo suvestine</a>
                </div>
            </div>
            <div class="container_3">
                <div>
                    <h3>Įvesti</h3>
                </div>
                <form action="form_1.php<?php if($id) : echo '?employee_id='.$id; endif;?>" name="form_1" method="<?php if($method=='edit') : echo 'get';else : echo 'post';endif;?>">
                    <?php
                        if ($method=='edit'):
                            echo '<input type="hidden" name="method" value="update">';
                            echo '<input type="hidden" name="employee_id" value="'.$id.'">';
                    endif;
                    ?>
                    <div>
                        <input type="text" name="employee_name" value="<?php
                            if($method == 'edit') :
                            echo $employee_name;
                            endif;
                            ?>" placeholder="Darbuotojo vardas">
                    </div>
                    <div>
                        <input type="text" name="employee_surname" value="<?php
                            if($method == 'edit') :
                            echo $employee_surname;
                            endif;
                            ?>" placeholder="Darbuotojo  pavardė">   
                    </div>
                    <div>
                        <button type="submit" name="save" value="ok">Išsaugoti</button>
                    </div>
                </form>
            </div>
            </form>
            <div class="container_3">
                <div>
                    <h3>Rezultatai</h3>
                </div>
                <div>
                    <table id='table_update'>
                        <tr>
                            <th>id</th>
                            <th>Vardas</th>
                            <th>Pavardė</th>
                        </tr>
                        <?php
                            //Kodas skirtas spausdinti employees lentele
                            foreach($table as $v)
                            {
                                    echo "<tr id='somerow'>";
                                    echo "<td>{$v[0]}</td>";
                                    echo "<td>{$v[1]}</td>";
                                    echo "<td>{$v[2]}</td>";
                                    echo '<td><a href="form_1.php?method=edit&employee_id='.$v[0].'&employee_name='.$v[1].'&employee_surname='.$v[2].'">Redaguoti</a></td>';
                                    echo '<td><a href="form_1.php?method=delete&employee_id='.$v[0].'">Ištrinti</a></td>';
                                    echo "<tr>";
                            }
                            ?>
                    </table>
                </div>
            </div>
        </main>
        <footer>
            <h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
        </footer>
    </body>
</html>