<?php
$user = 'root';
$pass = '';

try
{
	$dbh = new PDO('mysql:host=localhost;dbname=copysystem', $user, $pass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "<script>alert('Connected succesfully!');</script>";
}
catch(PDOException $e)
{
     echo "<script>alert('Connection failed!');</script>";
}

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<title>CopyX</title>
</head>
<body>

    <?php
    if(isset($_GET['save']))
    {
    $sql=$dbh->query("SELECT orders.id, CONCAT(employees.name,' ',employees.surname), orders.date, orders.total, SUM(orders.total) FROM orders INNER JOIN employees ON employees.id = orders.employee_id WHERE orders.employee_id = '$_GET[employee_id]' and orders.date = '$_GET[day]'");
    $table=$sql->FetchAll();
    }

    $select_sql = $dbh->query("SELECT * FROM employees");
    $select_employee= $select_sql->FetchAll();
    $dbh=null;
    ?>

	<header>
		<div class="container_1">
			<div class="box_1">
				<img src="img/logo.jpg" name="logo" id="logo">
			</div>
			<div class="box_2">
				<input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
			</div>
			<div class="box_3">
				<button type="button" id="search_btn">Ieškoti</button>
			</div>
		</div>
		<center>
			<nav id="navigacija">
				<a href="index.php">Formos ir ataskaitos</a> |
				<a href="information.php">Informacija</a> |
				<a href="contacts.php">Kontaktai</a>
			</nav>
		</center>
	</header>

		<main>
			<h1>Formos ir ataskaitos</h1>
			<div class="container_2">
				<div class="element_1">
					<a href="form_1.php">Įrašyti naują darbuotoją</a>
				</div>
				<div class="element_2">
					<a href="form_2.php">Įrašyti naują įmonę</a>
				</div>
				<div class="element_3">
					<a href="form_3.php">Įrašyti naują užsakymo būklę</a>
				</div>
				<div class="element_4">
					<a href="form_4.php">Dienos apskaita</a>
				</div>
				<div class="element_5" style="background-color: silver;">
					<a href="form_5.php">Grynų pinigų įnešimas pagal darbuotoją</a>
				</div>
				<div class="element_6">
					<a href="form_6.php">Firmos sąskaitos už mėnesį</a>
				</div >
				<div class="element_7">
					<a href="form_7.php">Firmos aptarnavimas pagal dieną</a>
				</div>
				<div class="element_8">
					<a href="form_8.php">Menesio darbo suvestine</a>
				</div>
			</div>
		
			<div class="container_3">
				<div><h3>Įvesti</h3></div>
				<form action="form_5.php" method="get">
				<div>
				<select  name="employee_id" placeholder="Darbuotojo vardas">
						<?php foreach($select_employee as $v) : ?>
    					<option value="<?php echo $v[0];?>"><?php echo "{$v[1]} {$v[2]}"?></option>
    					<?php endforeach ?>
				</select>
    			</div>
    			<div>
    				<input type="date" name="day" placeholder="Diena">
    			</div>
    			<div>
    				<button type="submit" name="save" value="ok">Ieškoti</button>
    			</div>
    		</form>
			</div>	
			
			<div class="container_3">
				<div><h3>Ataskaita (<?php echo date("Y-m-d", strtotime($_GET['month'])); ?>) </h3></div>
					<?php
					if (isset($_GET['save'])) :
					echo "<table><tr><th>id</th><th>Darbuotojas</th><th>Data</th><th>Suma</th></tr>";
						foreach($table as $v) :
    				echo "<tr>";
    				echo "<td>{$v[0]}</td>";
   					echo "<td>{$v[1]}</td>";
   					echo "<td>{$v[2]}</td>";
   					echo "<td>{$v[3]} €</td>";
    				echo "<tr>";
					endforeach;
					echo "</table>";
					echo "<div><h4>Iš viso įnešta: ",$v[4]," €</h4></div>";
					endif;
					?>
				</table>
			</div>
			
		</main>
	<footer>
		<h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
	</footer>
</body>
</html>