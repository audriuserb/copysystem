<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<title>CopyX</title>
</head>
<body>
	<header>
		<div class="container_1">
			<div class="box_1">
				<img src="img/logo.jpg" name="logo" id="logo">
			</div>
			<div class="box_2">
				<input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
			</div>
			<div class="box_3">
				<button type="button" id="search_btn">Ieškoti</button>
			</div>
		</div>
		<center>
			<nav id="navigacija">
				<a href="index.php">Formos ir ataskaitos</a> |
				<a href="information.php">Informacija</a> |
				<a href="contacts.php">Kontaktai</a>
			</nav>
		</center>
	</header>

		<main>
			<h1>Informacija</h1>
			<div class="container_7">
				<div class="info_element">
						<label>Visi mokėjimai atlikti už 06 mėnesį</label>
						<a href="form_4.html">Mėnesio ataskaita</a><br>
						<label>Jonas Jonaitis 2018.07.02</label>
				</div>
				<div class="info_element">
						<label>Užpildyta dienos apskaita</label>
						<a href="form_2.html">Dienos apskaita</a><br>
						<label>Jonas Jonaitis 2018.06.18</label>
				</div>
			</div>	
		</main>
	<footer>
		<h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
	</footer>
</body>
</html>