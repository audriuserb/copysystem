<?php
    $user = 'root';
    $pass = '';
    
    try
    {
        $dbh = new PDO('mysql:host=localhost;dbname=copysystem', $user, $pass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        //echo "<script>alert('Connected succesfully!');</script>";
    }
    
    catch(PDOException $e)
    {
         //echo "<script>alert('Connection failed!');</script>";
    }
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <title>CopyX</title>
    </head>
    <body>

    <?php
$table_sql = $dbh->query("SELECT * FROM order_statuses ORDER BY id");
        $table= $table_sql->FetchAll();
    
        $save = isset($_POST['save']) ? $_POST['save'] : '';
        $method = isset($_REQUEST['method']) ? $_REQUEST['method'] : '';
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        $title=isset($_REQUEST['title']) ? $_REQUEST['title'] : '';
        if ($method =='' and $save) $method='save';

        //lenteles order_statuses pildymas
        if ($method == 'save') 
            {
            $duplicate = $dbh->query("SELECT id FROM order_statuses WHERE NOT EXISTS (SELECT id FROM order_statuses WHERE title = '$title' LIMIT 1)");
            $result = $duplicate->fetch();
            if($result[0] != 0 and $id =='')
                {
                $sql_1 = $dbh->prepare("INSERT INTO order_statuses (title)  Values ('$title')");
                if($sql_1->execute())
                {
                echo "<script>alert('Duomenys apie užsakymų būklę išsaugoti');</script>";
                header("Refresh:0; url='form_3.php'");
                }
            else
                {
                echo "<script>alert('Tokia užsakymo būklė jau egzistuoja');</script>";
                }
            }
        }

            //lenteles order_statuses įrašų redagavimas
            if($method == 'update')
            {
            $sql_2 = $dbh->prepare("UPDATE order_statuses SET title = '$title' WHERE id = '$id'");
            if($sql_2->execute())
                {
                echo "<script>alert('Užsakymo būklės duomenys pakeisti');</script>";
                header("Refresh:0; url='form_3.php'");
                }
            else 
                {
                echo "<script>alert('Užsakymo būklės duomenų pakeisti nepavyko');</script>";
                }
            }

            //lenteles order_statuses įrašų pašalinimas
            else if($method == 'delete')
            {
                $sql_3=$dbh->prepare("DELETE FROM order_statuses where id = '$id'");
                if($sql_3->execute())
                {
                  echo "<script>alert('Užsakymo būklės duomenys ištrinti');</script>";
                  header("Refresh:0; url='form_3.php'"); 
                }
                else
                {
                    echo "<script>alert('Užsakymo būklės duomenų ištrinti nepavyko');</script>"; 
                }
            }

            $dbh=null;
    ?>
        <header>
            <div class="container_1">
                <div class="box_1">
                    <img src="img/logo.jpg" name="logo" id="logo">
                </div>
                <div class="box_2">
                    <input type="text" name="paieska" id="search" placeholder="&#x1F50E Paieška...">
                </div>
                <div class="box_3">
                    <button type="button" id="search_btn">Ieškoti</button>
                </div>
            </div>
            <center>
                <nav id="navigacija">
                    <a href="index.php">Formos ir ataskaitos</a> |
                    <a href="information.php">Informacija</a> |
                    <a href="contacts.php">Kontaktai</a>
                </nav>
            </center>
        </header>
        <main>
            <h1>Formos ir ataskaitos</h1>
            <div class="container_2">
                <div class="element_1">
                    <a href="form_1.php">Įrašyti naują darbuotoją</a>
                </div>
                <div class="element_2">
                    <a href="form_2.php">Įrašyti naują įmonę</a>
                </div>
                <div class="element_2" style="background-color: silver;">
                    <a href="form_3.php">Įrašyti naują užsakymo būklę</a>
                </div>
                <div class="element_3">
                    <a href="form_4.php">Dienos apskaita</a>
                </div>
                <div class="element_4">
                    <a href="form_5.php">Grynų pinigų įnešimas pagal darbuotoją</a>
                </div>
                <div class="element_5">
                    <a href="form_6.php">Firmos sąskaitos už mėnesį</a>
                </div >
                <div class="element_6">
                    <a href="form_7.php">Firmos aptarnavimas pagal dieną</a>
                </div>
                <div class="element_7">
                    <a href="form_8.php">Menesio darbo suvestine</a>
                </div>
            </div>
            <div class="container_3">
                <div>
                    <h3>Įvesti</h3>
                </div>
                <form action="form_3.php<?php if($id) : echo '?id='.$id; endif;?>" method="<?php if($method=='edit') : echo 'get'; else : echo 'post';endif;?>">
                    <?php
                        if ($method=='edit'):
                            echo '<input type="text" hidden="true" name="method" value="update">';
                            echo '<input type="number" hidden="true" name="id" value="'.$id.'">';
                    endif;
                    ?>
                    <div>
                        <input type="text" name="title" value="<?php if($method == 'edit') echo $title; ?>" placeholder="Užsakymo būklės pavadinimas">
                    </div>
                    <div>
                        <button type="submit" name="save" value="ok">Išsaugoti</button>
                    </div>
                </form>
            </div>
            <div class="container_3">
                <div>
                    <h3>Rezultatai</h3>
                </div>
                <div>
                    <table id='table_update'>
                        <tr>
                            <th>id</th>
                            <th>Užsakymo būklė
                        </tr>
                        <?php
                            //Kodas skirtas spausdinti employees lentele
                            foreach($table as $v)
                            {
                                    echo "<tr>";
                                    echo "<td>{$v[0]}</td>";
                                    echo "<td>{$v[1]}</td>";
                                    echo '<td><a href="form_3.php?method=edit&id='.$v[0].'&title='.$v[1].'">Redaguoti</a></td>';
                                    echo '<td><a href="form_3.php?method=delete&id='.$v[0].'">Ištrinti</a></td>';
                                    echo "</tr>";
                            }
                            ?>
                    </table>
                </div>
            </div>
        </main>
        <footer>
            <h6>Puslapį sukūrė vardaitis pavardaitis 2018</h6>
        </footer>
    </body>
</html>